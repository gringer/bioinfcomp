#!/usr/bin/env Rscript

L <- 15;
n <- 6;
p <- 0.005;
k <- 5;
## note: not tractable for k > ~4
res <- NULL;
tProb <- NULL;
## Set up coverage probability matrix
## Only done up to k=18; anything higher is too computationally
## intensive, and is beyond the expected error of the test
for(coverage in 1:min(k,18)){
  checkProbs <- data.frame(id=0:(2^coverage - 1));
  checkProbs$binary <- sapply(checkProbs$id, 
                              function(x){paste(as.integer(intToBits(x))[1:k],
                                                collapse="")});
  checkProbs$tCount <- sapply(checkProbs$id, 
                              function(x){sum(as.integer(intToBits(x)))});
  checkProbs$fCount <- coverage - checkProbs$tCount;
  checkProbs$freq <- (1-p)^checkProbs$tCount * p^checkProbs$fCount;
  checkProbs$call <- (-sign(checkProbs$tCount - checkProbs$fCount)+1)/2;
  tProb[coverage] <- sum(checkProbs$freq * checkProbs$call);
}
## for k>18, just treat the function as log-linear. 
## There will be some error, but not as much as if 
## coverage > 18 were treated as 18.
if(k > 18){
  lastDelta <- log(tProb[18]) - log(tProb[16]);
  for(coverage in 19:k){
    tProb[coverage] <- exp(log(tProb[coverage-2]) + lastDelta);
  }
}
## populate initial matrix (place first read)
states <- matrix(0,nrow = 1, ncol=L);
probs <- 1;
states[1,1:n] <- 1;
res[1] <- sum((apply(sign(states),1,sum) * p + 
                 (L - apply(sign(states),1,sum)) * 0.75) * probs);
## add second read
## Note: when coverage <= 2, p(2 reads) == p(1 read)
startState <- states[1,];
startProb <- probs[1];
if(k > 1){
  newArr <- NULL;
  newProbs <- NULL;
  for(start in 1:L){
    new <- startState;
    sRange <- (((start:(start+n-1)) - 1) %% L) + 1;
    new[sRange] <- new[sRange] + 1;
    newArr <- rbind(newArr, new);
    newProbs <- c(newProbs, startProb * 1/L);
  }
  states <- newArr;
  probs <- newProbs;
  res[2] <- sum((apply(sign(states),1,sum) * p + 
                   (L - apply(sign(states),1,sum)) * 0.75) * probs);
}
if(k > 2){
    ## add third read
    for(readCount in 3:k){
        cat(readCount,"... ",sep="");
        newArr <- NULL;
        newProbs <- NULL;
        for(il in 1:nrow(states)){
            startState <- states[il,];
            startProb <- probs[il];
            for(start in 1:L){
                new <- startState;
                sRange <- (((start:(start+n-1)) - 1) %% L) + 1;
                new[sRange] <- new[sRange] + 1;
                newArr <- rbind(newArr, new);
                newProbs <- c(newProbs, startProb * 1/L);
            }
        }
        ## collapse to remove duplicates
        newArr.collapsed <- apply(newArr,1,function(x){
          ## consider things that don't change the calculation:
          ## * reversal
          ## * rotation (i.e. shift out on left + shift in on right)
          ## choose a string that represents all of these, i.e.
          ## the lexicographically least
          l <- length(x);
          d.x <- c(x,x);
          d.rx <- rev(d.x);
          rots <- NULL;
          for(i in 1:length(x)){
            rots <- c(rots, 
                      paste(d.x[i:(i+l)], collapse=";"), 
                      paste(d.rx[i:(i+l)], collapse=";"));
          }
          min(rots);
        });
        probs.collapsed <- tapply(newProbs, newArr.collapsed, sum);
        states <- newArr[match(names(probs.collapsed),newArr.collapsed),];
        probs <- probs.collapsed;
        stateError <- states;
        stateError[states > 0] <- (tProb[stateError[states > 0]]);
        res[readCount] <- 
            sum((apply(stateError,1,sum) +
                 (L - apply(sign(states),1,sum)) * 0.75) * probs);
    }; cat("\n");
}
print(res);